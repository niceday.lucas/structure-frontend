# 프론트엔드 개발 예제
  * frontend 개발을 위한 기본 개발환경을 제공한다.  


# 이력
  * v0.0.1       
    * 2019.09.10
        * 패키지 구조 변경
    * 2019.09.01 
      * 최초 등록


# 구성

  <!-- blank line -->
  * 환경
    * mac high sierra+, linux, windows 10
    * webstorm 2019.1, visual studio code v1.36.1
    * sourcetree v2.7.6
    * node v10.16.0
    * npm v6.9.0

  <!-- blank line -->
  * 라이브러리
    * vue.js v2.6.10
      * https://kr.vuejs.org/v2/guide/index.html
    * typescript v3.5.3
      * https://www.typescriptlang.org
    * vue-class-component
      * https://github.com/vuejs/vue-class-component
    * class-validator
      * https://github.com/typestack/class-validator
    * class-transformer
      * https://github.com/typestack/class-transformer
    * vuex-smart-module
      * https://github.com/ktsn/vuex-smart-module
    * vue-property-decorator
      * https://github.com/kaorun343/vue-property-decorator
    * axios
      * https://github.com/axios/axios
    * lodash
      * https://lodash.com
     

# 실행
  * npm install
  * npm run serve

  
# 개발
  ## 패키지 구조   
  | 패키지 | 설명 |
  | --------           |  --------                                            |
  | io.niceday.common  | 공통 패키지(비니지스 로직이 포함되지 않는 상위 영역) |
  | io.niceday.sample  | 샘플 패키지(기본 CRUD)            |

  ## 기능별 공통 패키지 구조
  | 패키지 | 설명 |
  |  --------                   |  --------            |
  | io.niceday.sample.model     | 모델 영역            |
  | io.niceday.sample.view      | 라우팅 컴포넌트 영역 |
  | io.niceday.sample.component | 자식 컴포넌트 영역   |
  | io.niceday.sample.router    | 라우팅 영역          |
  | io.niceday.sample.state     | 상태 영역            |
  | io.niceday.sample.action    | 제어 영역            |
  | io.niceday.sample.mutation  | 상태 변경 영역       |
  | io.niceday.sample.getter    | 상태 반환 영역       |
  | io.niceday.sample.store     | 저장소 영역          |