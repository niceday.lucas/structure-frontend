export function Description(name : string) { 

    return (target: any, propertyKey: string | symbol) : void => {

        const metaValue: Map<string, string> = target.$description === undefined ? new Map() : target.$description;
        metaValue.set(propertyKey as string, name);

        Object.defineProperty(target, '$description', {
            value: metaValue,
            configurable: true
        });
    };
}