export function Validate(target : any, propertyKey : string, descriptor : PropertyDescriptor) {
    const original   = descriptor.value;
    descriptor.value = function (...args : any) {
        Object.$plugins.validator.validate(args[0]).then(errors => {
            return Object.$plugins.lodash.isEmpty(errors) ? original.apply(this, args) : console.log(errors);
        });
    }
}