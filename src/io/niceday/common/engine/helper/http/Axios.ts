import AxiosHttp      from 'axios';
import {SpinnerEvent} from '@/io/niceday/common/engine/event/SpinnerEvent';

const axios = AxiosHttp.create({
     baseURL : 'http://localhost:8080/api'
    ,timeout : 10000
});

axios.interceptors.request.use (
    (conf) => {
        SpinnerEvent.$emit('before-request');
        return conf;
    },
    (error) => {
        SpinnerEvent.$emit('request-error');
        return Promise.reject(error);
    }
);

axios.interceptors.response.use (
    (response) => {
        SpinnerEvent.$emit('after-response');
        return response;
    },
    (error) => {
        SpinnerEvent.$emit('response-error');
        return Promise.reject(error);
    }
);

export default axios;