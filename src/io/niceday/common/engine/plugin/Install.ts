import * as lodash    from 'lodash';
import * as validator from 'class-validator';
import {Mapper}       from '@/io/niceday/common/engine/plugin/custom/Mapper';
import {Plugin}       from '@/io/niceday/common/engine/plugin/Plugin';

export default {

   install() {
       Object.defineProperty(Object, '$plugins', {
           get : () : Plugin => {
               return new Plugin (
                    new Mapper
                   ,new validator.Validator
                   ,Object.create(lodash)
               );
           }
       });
   }
}
