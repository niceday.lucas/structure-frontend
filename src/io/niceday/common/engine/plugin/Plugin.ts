import * as validator from 'class-validator';
import * as lodash    from 'lodash';
import {Mapper}       from '@/io/niceday/common/engine/plugin/custom/Mapper';

declare global { 
    interface Object {
        $plugins : IPlugin;
    }

    interface IPlugin {
        mapper    : Mapper<any>;
        validator : validator.Validator;
        lodash    : lodash.LoDashStatic;
    }
}

export class Plugin implements IPlugin {

    constructor(mapper : Mapper<any>, validator : validator.Validator, lodash:lodash.LoDashStatic){
        this.mapper    = mapper;
        this.validator = validator;
        this.lodash    = lodash;
    }

    public lodash!    : lodash.LoDashStatic;
    public mapper!    : Mapper<any>;
    public validator! : validator.Validator;
}