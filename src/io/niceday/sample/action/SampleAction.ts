import {Actions}        from 'vuex-smart-module';
import router           from '@/router';
import http             from '@/io/niceday/common/engine/helper/http/Axios';
import {Validate}       from '@/io/niceday/common/engine/decorator/Validate';  
import {SampleState}    from '@/io/niceday/sample/state/SampleState';  
import {SampleGetter}   from '@/io/niceday/sample/getter/SampleGetter';  
import {SampleMutation} from '@/io/niceday/sample/mutation/SampleMutation';  
import {Sample}         from '@/io/niceday/sample/model/Sample';     

export class SampleAction extends Actions<SampleState, SampleGetter, SampleMutation, SampleAction> {

    public getPage(search : Sample.Request.Search) {
        http.get(`/samples/pages?sort=id,desc`, {params : search}).then(response => {
            this.mutations.setSearch(search);
            this.mutations.setPage  (response.data);
        });
    }

    public get(sampleId : number | string) {
        http.get(`/samples/${sampleId}`).then(response => this.mutations.set(response.data));
    }

    @Validate
    public add(sample : Sample.Request.Add) {
        http.post(`/samples`, sample).then(() => router.push('/samples/list'));
    }

    @Validate
    public modify(sample : Sample.Request.Modify) {
        http.put(`/samples/${sample.id}`, sample).then(() => router.push('/samples/list'));
    }

    public remove(sampleId : number | string) {
        http.delete(`/samples/${sampleId}`).then(() => router.push('/samples/list'));
    }
}