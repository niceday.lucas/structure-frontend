import {Getters}     from 'vuex-smart-module';
import {Common}      from '@/io/niceday/common/engine/model/Common';
import {Sample}      from '@/io/niceday/sample/model/Sample';     
import {SampleState} from '@/io/niceday/sample/state/SampleState';  

export class SampleGetter extends Getters<SampleState> {

    public get() : Sample.Response.FindOne {
        return Object.$plugins.mapper.toObject(Sample.Response.FindOne, this.state.sample);
    }

    public getPage() : Common.Response.Page {
        return Object.$plugins.mapper.toPage(Sample.Response.FindAll, this.state.page);
    }

    public getModify() : Sample.Request.Modify {
        return Object.$plugins.mapper.toObject(Sample.Request.Modify, this.state.sample);
    }
}