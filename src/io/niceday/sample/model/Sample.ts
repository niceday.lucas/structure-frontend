import                     'reflect-metadata';
import {Expose}       from 'class-transformer';
import {IsString}     from 'class-validator';
import {IsDate}       from 'class-validator';
import {IsBoolean}    from 'class-validator';
import {IsNumber}     from 'class-validator';
import {IsNotEmpty}   from 'class-validator';
import {MaxLength}    from 'class-validator';
import {Common}       from '@/io/niceday/common/engine/model/Common';
import {Description}  from '@/io/niceday/common/engine/decorator/Description';

export namespace Sample {
    export namespace Request {
        export class Search extends Common.Request.Page {

            @Expose() @Description('제목')
            public title!: string;
        }

        export class Add {

            @Expose() @Description('사용자아이디')
            @IsNotEmpty() 
            public userId!: string;
            
            @Expose() @Description('제목')
            @IsString() @IsNotEmpty() @MaxLength(20)
            public title! : string;
        
            @Expose() @Description('내용')
            @IsNotEmpty() 
            public content!: string;
        }

        export class Modify {
            
            @Expose() @Description('샘플일련번호') 
            @IsNumber()
            public id!: number;

            @Expose() @Description('사용자아이디')
            @IsNotEmpty() 
            public userId!: string;
            
            @Expose() @Description('제목')
            @IsString() @IsNotEmpty() @MaxLength(20)
            public title!: string;
        
            @Expose() @Description('내용')
            @IsNotEmpty()
            public content!: string;
        }
    }

    export namespace Response {
        export class FindAll {
            
            @Expose() @Description('샘플일련번호')
            public id!: number;

            @Expose() @Description('사용자아이디')
            public userId!: string;
            
            @Expose() @Description('제목')
            public title!: string;
        
            @Expose() @Description('내용')
            public content!: string;

            @Expose() @Description('등록일시')
            public createdAt!: Date;

            @Expose() @Description('수정일시')
            public updatedAt!: Date;

            @Expose() @Description('등록자')
            public creator !: Common.Response.Account;

            @Expose() @Description('수정자')
            public updator !: Common.Response.Account;
        }

        export class FindOne {
            
            @Expose() @Description('샘플일련번호')
            public id!: number;

            @Expose() @Description('사용자아이디')
            public userId!: string;
            
            @Expose() @Description('제목')
            public title!: string;
        
            @Expose() @Description('내용')
            public content!: string;

            @Expose() @Description('등록일시')
            public createdAt!: Date;

            @Expose() @Description('수정일시')
            public updatedAt!: Date;

            @Expose() @Description('등록자')
            public creator !: Common.Response.Account;

            @Expose() @Description('수정자')
            public updator !: Common.Response.Account;
        }
    }
}