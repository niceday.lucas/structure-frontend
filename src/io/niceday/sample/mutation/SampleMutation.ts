import {Mutations}   from 'vuex-smart-module';
import {Common}      from '@/io/niceday/common/engine/model/Common';
import {Sample}      from '@/io/niceday/sample/model/Sample';     
import {SampleState} from '@/io/niceday/sample/state/SampleState';  

export class SampleMutation extends Mutations<SampleState> { 

    public setSearch(search : Sample.Request.Search) : void {
        this.state.search = Object.$plugins.mapper.toObject(Sample.Request.Search, search);
    }

    public setPage(page : Common.Response.Page) : void {
        this.state.page = Object.$plugins.mapper.toPage(Sample.Response.FindAll, page);
    }
    
    public set(sample : Sample.Response.FindOne) : void {
        this.state.sample = Object.$plugins.mapper.toObject(Sample.Response.FindOne, sample);
    }
}