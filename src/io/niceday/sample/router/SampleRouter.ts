const SampleRouter = [
    {
         path      : '/'
        ,component : () => import('@/io/niceday/sample/view/SampleList.vue')
    },
    {
         path      : '/samples/list'
        ,component : () => import('@/io/niceday/sample/view/SampleList.vue')
    },
    {
         path      : '/samples/add'
        ,component : () => import('@/io/niceday/sample/view/SampleAdd.vue')
    },
    {
         path      : '/samples/modify/:id'
        ,component : () => import('@/io/niceday/sample/view/SampleModify.vue')
    },
    {
         path      : '/samples/view/:id'
        ,component : () => import('@/io/niceday/sample/view/SampleView.vue')
    }
];

export default SampleRouter;