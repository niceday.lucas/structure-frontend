import {Sample} from '@/io/niceday/sample/model/Sample';     
import {Common} from '@/io/niceday/common/engine/model/Common';

export class SampleState { 
    
    public search : Sample.Request.Search   = new Sample.Request.Search;
    public page   : Common.Response.Page    = new Common.Response.Page;
    public sample : Sample.Response.FindOne = new Sample.Response.FindOne;
}