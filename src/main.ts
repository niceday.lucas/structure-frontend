import Vue          from "vue";
import NicedayApp   from "@/NicedayApp.vue";
import router       from '@/router';
import Install      from "@/io/niceday/common/engine/plugin/Install";

Vue.config.productionTip = false;
Vue.use(Install);

new Vue({
     router
    ,render : h => h(NicedayApp)
})
.$mount("#app");