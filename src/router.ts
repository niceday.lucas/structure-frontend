import Vue           from "vue";
import Router        from "vue-router";
import SampleRouter  from '@/io/niceday/sample/router/SampleRouter';

Vue.use(Router);

export default new Router({
     mode   : "history"
    ,routes : [...SampleRouter]
});